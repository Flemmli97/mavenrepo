### Maven Repo for my stuff

To use add this to your build.gradle:

```groovy
repositories {
    ...
    maven {
        url "https://gitlab.com/api/v4/projects/21830712/packages/maven"
    }
}
```
